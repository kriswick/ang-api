package com.kriswick.angapi.model;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

public interface ToDoItemResource {

    String PATH = "/todo";

    @RequestMapping(
            path = PATH,
            method = POST,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    @ResponseStatus(OK)
    // Ideally the response should be HTTP 201 (created) but the specification at
    // https://join.autogeneral.com.au/swagger-ui/?url=/swagger.json#/todo/post_todo requires it to be 200
    ToDoItem create(ToDoItemAddRequest toDoItemAddRequest);

    @RequestMapping(
            path = PATH + "/{id}",
            method = GET,
            produces = APPLICATION_JSON_VALUE
    )
    ToDoItem get(@PathVariable("id") Long id);

    @RequestMapping(
            path = PATH + "/{id}",
            method = PATCH,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    ToDoItem update(
            @PathVariable("id") Long id,
            @RequestBody ToDoItemUpdateRequest toDoItemUpdateRequest
    );
}
