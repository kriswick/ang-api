package com.kriswick.angapi.model;

import javax.validation.constraints.Size;

public final class ToDoItemUpdateRequest {

    @Size(min=1, max = 50, message = "Must be between 1 and 50 chars long")
    private String text;

    private Boolean isCompleted;

    public ToDoItemUpdateRequest(String text, Boolean isCompleted) {
        this.text = text;
        this.isCompleted = isCompleted;
    }

    //For Jackson
    private ToDoItemUpdateRequest(){}

    public String getText() {
        return text;
    }

    public Boolean getIsCompleted() {
        return isCompleted;
    }

}
