package com.kriswick.angapi.model;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Size;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

public interface TaskResource {

    String PATH = "/tasks";

    @RequestMapping(
            path = PATH + "/validateBrackets",
            method = GET,
            produces = APPLICATION_JSON_VALUE
    )
    public BalanceTestResult validateBrackets(
        @Size(min = 1, max = 100, message = "Must be between 1 and 100 chars long") @RequestParam("input") String input
    );
}
