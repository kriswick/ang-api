package com.kriswick.angapi.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public final class ToDoItemAddRequest {

    @NotEmpty
    @Size(min=1, max = 50, message = "Must be between 1 and 50 chars long")
    private String text;

    public ToDoItemAddRequest(String text) {
        this.text = text;
    }

    //For Jackson
    private ToDoItemAddRequest(){ }

    public String getText() {
        return text;
    }

}
