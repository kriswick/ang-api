package com.kriswick.angapi.model.error;

import java.util.List;

public final class ApiError {

    private String name;

    private List<ErrorDetails> details;

    public ApiError(String name, List<ErrorDetails> details) {
        this.name = name;
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public List<ErrorDetails> getDetails() {
        return details;
    }

}
