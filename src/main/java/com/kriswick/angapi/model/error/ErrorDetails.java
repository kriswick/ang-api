package com.kriswick.angapi.model.error;

public class ErrorDetails {

    private String message;

    public ErrorDetails(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
