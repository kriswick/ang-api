package com.kriswick.angapi.model.error;

public final class ValidationErrorDetails extends ErrorDetails {

    private String location = "params";

    private String param;

    private String value;

    public ValidationErrorDetails(String message, String location, String param, String value) {
        super(message);
        this.location = location;
        this.param = param;
        this.value = value;
    }

    public String getLocation() {
        return location;
    }

    public String getParam() {
        return param;
    }

    public String getValue() {
        return value;
    }

}
