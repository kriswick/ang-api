package com.kriswick.angapi.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

public final class ToDoItem {

    @NotNull
    private final Long id;

    @NotNull
    @Size(min=1, max = 50)
    private final String text;

    @NotNull
    private final Boolean isCompleted;

    @NotNull
    private final ZonedDateTime createdAt;

    public ToDoItem(Long id, String text, Boolean isCompleted, ZonedDateTime createdAt) {
        this.id = id;
        this.text = text;
        this.isCompleted = isCompleted;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Boolean getIsCompleted() {
        return isCompleted;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

}
