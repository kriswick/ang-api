package com.kriswick.angapi.model;

public final class BalanceTestResult {

    private String input;

    private Boolean isBalanced;

    public BalanceTestResult(String input, Boolean isBalanced) {
        this.input = input;
        this.isBalanced = isBalanced;
    }

    public String getInput() {
        return input;
    }

    public Boolean getIsBalanced() {
        return isBalanced;
    }

}
