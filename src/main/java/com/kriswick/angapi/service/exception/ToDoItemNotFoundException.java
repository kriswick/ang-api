package com.kriswick.angapi.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ToDoItemNotFoundException extends ApiException {

    public ToDoItemNotFoundException(Long id){
        super("todo.not.found", String.format("Item with id %d not found", id));
    }
}
