package com.kriswick.angapi.service;

import com.kriswick.angapi.db.entity.ToDoItemEntity;
import com.kriswick.angapi.db.repository.ToDoItemRepository;
import com.kriswick.angapi.model.ToDoItem;
import com.kriswick.angapi.model.ToDoItemAddRequest;
import com.kriswick.angapi.model.ToDoItemUpdateRequest;
import com.kriswick.angapi.service.exception.ToDoItemNotFoundException;
import com.kriswick.angapi.service.mapper.ToDoItemMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.kriswick.angapi.service.mapper.ToDoItemMapper.toEntity;
import static com.kriswick.angapi.service.mapper.ToDoItemMapper.toModel;

@Service
public class ToDoItemServiceImpl implements ToDoItemService {

    private final ToDoItemRepository toDoItemRepository;

    public ToDoItemServiceImpl(ToDoItemRepository toDoItemRepository) {
        this.toDoItemRepository = toDoItemRepository;
    }

    @Override
    @Transactional
    public ToDoItem createToDoItem(ToDoItemAddRequest toDoItemAddRequest) {
        //Any business rule validations would go here
        ToDoItemEntity toDoItemEntity = toDoItemRepository.save(toEntity(toDoItemAddRequest));

        return toModel(toDoItemEntity);
    }

    @Override
    public ToDoItem getToDoItem(Long id) {
        ToDoItemEntity toDoItemEntity = toDoItemRepository.findById(id)
                .orElseThrow(() -> new ToDoItemNotFoundException(id));
        return toModel(toDoItemEntity);
    }

    @Override
    @Transactional
    public ToDoItem updateToDoItem(Long id, ToDoItemUpdateRequest toDoItemUpdateRequest) {
        ToDoItemEntity toDoItemEntity = toDoItemRepository.findById(id)
                .orElseThrow(() -> new ToDoItemNotFoundException(id));
        toDoItemEntity = ToDoItemMapper.updateToDoItem(toDoItemEntity, toDoItemUpdateRequest);

        return toModel(toDoItemEntity);
    }


}
