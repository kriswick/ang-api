package com.kriswick.angapi.service;

import com.kriswick.angapi.model.BalanceTestResult;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {

    @Override
    public BalanceTestResult validateBrackets(String input) {
        ConcurrentLinkedDeque<Character> bracketSegments = new ConcurrentLinkedDeque<>();

        List<Character> inputChars = input.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        boolean valid = true;

        for (Character c : inputChars) {
            if (isOpenBracketCharacter(c)) {
                bracketSegments.push(c);
            } else if (isCloseBracketCharacter(c)) {
                try {
                    Character lastOpenBracket = bracketSegments.pop();
                    if (isMatchingOpeningAndClosingBracketCharacters(lastOpenBracket, c)) {
                        valid = false;
                        break;
                    }
                } catch (NoSuchElementException e) {
                    valid = false;
                    break;
                }
            }
        }

        if (valid) {
            valid = bracketSegments.isEmpty();
        }

        return new BalanceTestResult(input, valid);
    }

    private boolean isMatchingOpeningAndClosingBracketCharacters(
            Character openBracketCharacter, Character closeBracketCharacter
    ) {
        return (closeBracketCharacter == ')' && openBracketCharacter != '(')
                || (closeBracketCharacter == '}' && openBracketCharacter != '{')
                || (closeBracketCharacter == ']' && openBracketCharacter != '[');
    }

    private boolean isCloseBracketCharacter(Character c) {
        return c == ')' || c == '}' || c == ']';
    }

    private boolean isOpenBracketCharacter(Character c) {
        return c == '(' || c == '{' || c == '[';
    }
}
