package com.kriswick.angapi.service;

import com.kriswick.angapi.model.BalanceTestResult;

public interface TaskService {
    BalanceTestResult validateBrackets(String input);
}
