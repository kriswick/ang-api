package com.kriswick.angapi.service;

import com.kriswick.angapi.model.ToDoItem;
import com.kriswick.angapi.model.ToDoItemAddRequest;
import com.kriswick.angapi.model.ToDoItemUpdateRequest;

public interface ToDoItemService {

    ToDoItem createToDoItem(ToDoItemAddRequest toDoItemAddRequest);

    ToDoItem getToDoItem(Long id);

    ToDoItem updateToDoItem(Long id, ToDoItemUpdateRequest toDoItemUpdateRequest);
}
