package com.kriswick.angapi.service.mapper;

import com.kriswick.angapi.db.entity.ToDoItemEntity;
import com.kriswick.angapi.model.ToDoItem;
import com.kriswick.angapi.model.ToDoItemAddRequest;
import com.kriswick.angapi.model.ToDoItemUpdateRequest;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class ToDoItemMapper {

    public static ToDoItemEntity toEntity(ToDoItemAddRequest toDoItemAddRequest) {
        ToDoItemEntity entity = new ToDoItemEntity();
        entity.setText(toDoItemAddRequest.getText());
        entity.setCompleted(false);
        entity.setCreatedAt(LocalDateTime.now());

        return entity;
    }

    public static ToDoItem toModel(ToDoItemEntity entity) {
        return new ToDoItem(
                entity.getId(),
                entity.getText(),
                entity.getCompleted(),
                entity.getCreatedAt().atZone(ZoneOffset.UTC)
        );
    }

    public static ToDoItemEntity updateToDoItem(ToDoItemEntity entity, ToDoItemUpdateRequest updateRequest) {
        if (updateRequest.getText() != null) {
            entity.setText(updateRequest.getText());
        }
        if (updateRequest.getIsCompleted() != null) {
            entity.setCompleted(updateRequest.getIsCompleted());
        }

        return entity;
    }
}
