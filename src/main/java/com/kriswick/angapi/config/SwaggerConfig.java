package com.kriswick.angapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.ZonedDateTime;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

    @Bean
    public Docket angApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.kriswick.angapi.controller"))
                .paths(regex("/todo.*|/tasks.*"))
                .build()
                .useDefaultResponseMessages(false)
                .directModelSubstitute(ZonedDateTime.class, String.class)
                .apiInfo(new ApiInfoBuilder()
                        .title("A&G Test REST API")
                        .description("\"This API implements the /todo and /task/validateBrackets endpoints " +
                                "specified in https://join.autogeneral.com.au/\"")
                        .contact(new Contact("Bhathiya Wickremasinghe", null, "bhathiyaw@gmail.com"))
                        .build());

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}