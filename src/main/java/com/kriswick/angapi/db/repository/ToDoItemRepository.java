package com.kriswick.angapi.db.repository;

import com.kriswick.angapi.db.entity.ToDoItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ToDoItemRepository extends JpaRepository<ToDoItemEntity, Long> {
}
