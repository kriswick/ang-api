package com.kriswick.angapi.controller;

import com.kriswick.angapi.model.error.ApiError;
import com.kriswick.angapi.model.error.ErrorDetails;
import com.kriswick.angapi.model.error.ValidationErrorDetails;
import com.kriswick.angapi.service.exception.ToDoItemNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.stream.Collectors;

@ControllerAdvice
public class ApiExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public ApiError handleBeanValidationMethodArgumentNotValidException(
            MethodArgumentNotValidException e
    ) {
        return new ApiError(
                "ValidationError",
                e.getBindingResult()
                    .getFieldErrors()
                    .stream()
                    .map(it -> new ValidationErrorDetails(
                            it.getDefaultMessage(),
                            "params",
                            it.getField(),
                            it.getRejectedValue().toString()
                    )).collect(Collectors.toList())
        );
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = ConstraintViolationException.class)
    @ResponseBody
    public ApiError handleConstraintViolationException(
            ConstraintViolationException e
    ) {
        return new ApiError(
                "ValidationError",
                e.getConstraintViolations()
                    .stream()
                    .map(it -> new ValidationErrorDetails(
                            it.getMessage(),
                            "params",
                            "",
                            it.getInvalidValue().toString()
                    )).collect(Collectors.toList())
        );
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = ToDoItemNotFoundException.class)
    @ResponseBody
    public ApiError handleToDoItemNotFoundException(
            ToDoItemNotFoundException e
    ) {
        return new ApiError(
                "NotFoundError",
                Collections.singletonList(
                        new ErrorDetails(e.getMessage())
                )
        );
    }
}
