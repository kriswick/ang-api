package com.kriswick.angapi.controller;

import com.kriswick.angapi.model.BalanceTestResult;
import com.kriswick.angapi.model.TaskResource;
import com.kriswick.angapi.model.ToDoItem;
import com.kriswick.angapi.model.error.ApiError;
import com.kriswick.angapi.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController(TaskResource.PATH)
@Validated
@Api(tags = "Tasks", description = "General algorithmic tasks")
public class TaskController implements TaskResource {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ToDoItem.class),
            @ApiResponse(code = 400, message = "Validation error", response = ApiError.class)
    })
    public BalanceTestResult validateBrackets(@RequestParam String input) {
        return taskService.validateBrackets(input);
    }
}
