package com.kriswick.angapi.controller;


import com.kriswick.angapi.model.ToDoItem;
import com.kriswick.angapi.model.ToDoItemAddRequest;
import com.kriswick.angapi.model.ToDoItemResource;
import com.kriswick.angapi.model.ToDoItemUpdateRequest;
import com.kriswick.angapi.model.error.ApiError;
import com.kriswick.angapi.service.ToDoItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController(ToDoItemResource.PATH)
@Api(tags = "Todos", description = "API to manage you todo list")
public class ToDoItemController implements ToDoItemResource {

    private final ToDoItemService toDoItemService;

    public ToDoItemController(ToDoItemService toDoItemService){
        this.toDoItemService = toDoItemService;
    }

    @Override
    @ApiOperation(value = "Create a ToDo item",response = ToDoItem.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created ToDo item"),
            @ApiResponse(code = 400, message = "Invalid input", response = ApiError.class)
    })
    public ToDoItem create(@Valid @RequestBody ToDoItemAddRequest toDoItemAddRequest) {
        return toDoItemService.createToDoItem(toDoItemAddRequest);
    }

    @Override
    @ApiOperation(value = "Retrieve a ToDo item",response = ToDoItem.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ToDoItem.class),
            @ApiResponse(code = 404, message = "ToDo item not found", response = ApiError.class)
    })
    public ToDoItem get(@PathVariable Long id) {
        return toDoItemService.getToDoItem(id);
    }

    @Override
    @ApiOperation(value = "Retrieve a ToDo item",response = ToDoItem.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated ToDo item", response = ToDoItem.class),
            @ApiResponse(code = 400, message = "Invalid input", response = ApiError.class),
            @ApiResponse(code = 404, message = "ToDo item not found", response = ApiError.class)
    })
    public ToDoItem update(
            @PathVariable Long id,
            @Valid @RequestBody ToDoItemUpdateRequest toDoItemUpdateRequest
    ) {
        return toDoItemService.updateToDoItem(id, toDoItemUpdateRequest);
    }

}
