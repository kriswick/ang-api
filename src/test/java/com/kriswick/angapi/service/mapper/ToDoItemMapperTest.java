package com.kriswick.angapi.service.mapper;

import com.kriswick.angapi.db.entity.ToDoItemEntity;
import com.kriswick.angapi.model.ToDoItem;
import com.kriswick.angapi.model.ToDoItemAddRequest;
import com.kriswick.angapi.model.ToDoItemUpdateRequest;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static com.kriswick.angapi.service.mapper.ToDoItemMapper.toEntity;
import static com.kriswick.angapi.service.mapper.ToDoItemMapper.toModel;
import static com.kriswick.angapi.service.mapper.ToDoItemMapper.updateToDoItem;
import static com.kriswick.angapi.test.factory.ToDoTestObjectFactory.toDoItemAddRequest;
import static com.kriswick.angapi.test.factory.ToDoTestObjectFactory.toDoItemEntity;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

public class ToDoItemMapperTest {

    @Test
    public void toDoItemAddRequestToEntityReturnsEntityWithGivenInformationAndDefaults(){
        ToDoItemAddRequest addRequest = toDoItemAddRequest("Send the link to this api");

        ToDoItemEntity toDoItemEntity = toEntity(addRequest);

        assertThat(toDoItemEntity.getText(), equalTo(addRequest.getText()));
        assertThat(toDoItemEntity.getCompleted(), equalTo(false));
        assertThat(toDoItemEntity.getCreatedAt(), lessThanOrEqualTo(LocalDateTime.now()));
        assertThat(toDoItemEntity.getCreatedAt(), greaterThan(LocalDateTime.now().minusSeconds(1)));
    }

    @Test
    public void toDoItemEntityToModelReturnsAModelPopulatedWithEntityInformation(){
        ToDoItemEntity entity = toDoItemEntity();

        ToDoItem toDoItem = toModel(entity);

        assertThat(toDoItem.getId(), equalTo(entity.getId()));
        assertThat(toDoItem.getText(), equalTo(entity.getText()));
        assertThat(toDoItem.getIsCompleted(), equalTo(entity.getCompleted()));
        assertThat(toDoItem.getCreatedAt(), equalTo(entity.getCreatedAt().atZone(ZoneOffset.UTC)));
    }

    @Test
    public void updateDoesNothingIfTheUpdateRequestIsEmpty(){
        ToDoItemUpdateRequest updateRequest = new ToDoItemUpdateRequest(null, null);
        ToDoItemEntity entity = toDoItemEntity();
        String originalText = entity.getText();
        boolean originalCompleted = entity.getCompleted();

        ToDoItemEntity updatedEntity = updateToDoItem(entity, updateRequest);

        assertThat(updatedEntity.getText(), equalTo(originalText));
        assertThat(updatedEntity.getCompleted(), equalTo(originalCompleted));
    }

    @Test
    public void updateUpdatesTheToDoItemWhenValuesArePresent(){
        ToDoItemUpdateRequest updateRequest = new ToDoItemUpdateRequest("new text", true);
        ToDoItemEntity entity = toDoItemEntity();

        ToDoItemEntity updatedEntity = updateToDoItem(entity, updateRequest);

        assertThat(updatedEntity.getText(), equalTo(updateRequest.getText()));
        assertThat(updatedEntity.getCompleted(), equalTo(updateRequest.getIsCompleted()));
    }
}