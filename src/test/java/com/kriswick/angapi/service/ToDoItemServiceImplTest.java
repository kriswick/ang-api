package com.kriswick.angapi.service;

import com.kriswick.angapi.db.entity.ToDoItemEntity;
import com.kriswick.angapi.db.repository.ToDoItemRepository;
import com.kriswick.angapi.model.ToDoItem;
import com.kriswick.angapi.model.ToDoItemAddRequest;
import com.kriswick.angapi.model.ToDoItemUpdateRequest;
import com.kriswick.angapi.service.exception.ToDoItemNotFoundException;
import com.kriswick.angapi.service.mapper.ToDoItemMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.ZoneOffset;
import java.util.Optional;
import java.util.Random;

import static com.kriswick.angapi.test.factory.ToDoTestObjectFactory.toDoItemAddRequest;
import static com.kriswick.angapi.test.factory.ToDoTestObjectFactory.toDoItemEntity;
import static com.kriswick.angapi.test.factory.ToDoTestObjectFactory.toDoItemUpdateRequest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ToDoItemServiceImplTest {

    @InjectMocks
    private ToDoItemServiceImpl toDoItemService;

    @Mock
    private ToDoItemRepository toDoItemRepository;

    @Spy
    private ToDoItemMapper toDoItemMapper = new ToDoItemMapper();

    @Test
    public void createToDoItemWithValidInputPersistsANewToDoAndReturnsTheSame(){
        ToDoItemAddRequest addRequest = toDoItemAddRequest("Test the service");

        when(toDoItemRepository.save(any(ToDoItemEntity.class))).thenAnswer(it -> {
            ToDoItemEntity entity = it.getArgument(0);
            ReflectionTestUtils.setField(entity, "id", new Random().nextLong());
            return  entity;
        });

        ToDoItem toDoItem = toDoItemService.createToDoItem(addRequest);

        assertThat(toDoItem, notNullValue());
        assertThat(toDoItem.getText(), equalTo(addRequest.getText()));
        assertThat(toDoItem.getId(), notNullValue());
        assertThat(toDoItem.getIsCompleted(), equalTo(false));
        assertThat(toDoItem.getCreatedAt(), notNullValue());
    }

    @Test(expected = ToDoItemNotFoundException.class)
    public void getToDoItemThrowsNotFoundExceptionWhenGivenIdDoesNotExist(){
        when(toDoItemRepository.findById(any())).thenReturn(Optional.empty());

        toDoItemService.getToDoItem(1L);
    }

    @Test
    public void getToDoItemReturnsTheCorrectToDoItemWhenFound(){
        Long toDoItemId = 1L;
        when(toDoItemRepository.findById(toDoItemId)).thenReturn(Optional.of(toDoItemEntity(toDoItemId)));

        ToDoItem toDoItem = toDoItemService.getToDoItem(toDoItemId);

        assertThat(toDoItem, notNullValue());
        assertThat(toDoItem.getId(), equalTo(toDoItemId));
    }

    @Test(expected = ToDoItemNotFoundException.class)
    public void updateToDoItemThrowsNotFoundExceptionWhenGivenIdDoesNotExist(){
        when(toDoItemRepository.findById(any())).thenReturn(Optional.empty());

        toDoItemService.updateToDoItem(1L, toDoItemUpdateRequest("a", true));
    }

    @Test
    public void updateToDoItemUpdatesAndReturnsTheToDoItem(){
        Long toDoItemId = 1L;
        ToDoItemEntity toDoItemEntity = toDoItemEntity(toDoItemId);
        when(toDoItemRepository.findById(toDoItemId)).thenReturn(Optional.of(toDoItemEntity));

        ToDoItemUpdateRequest updateRequest = toDoItemUpdateRequest("b", true);
        ToDoItem toDoItem = toDoItemService.updateToDoItem(toDoItemId, updateRequest);

        assertThat(toDoItem, notNullValue());
        assertThat(toDoItem.getText(), equalTo(updateRequest.getText()));
        assertThat(toDoItem.getId(), equalTo(toDoItemEntity.getId()));
        assertThat(toDoItem.getIsCompleted(), equalTo(updateRequest.getIsCompleted()));
        assertThat(toDoItem.getCreatedAt(), equalTo(toDoItemEntity.getCreatedAt().atZone(ZoneOffset.UTC)));
    }
}