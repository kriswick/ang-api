package com.kriswick.angapi.service;

import com.kriswick.angapi.model.BalanceTestResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(Parameterized.class)
public class TaskServiceValiateBracketTest {

    private String scenario;
    private String input;
    private Boolean expectedMatched;

    private TaskService taskService = new TaskServiceImpl();

    public TaskServiceValiateBracketTest(String scenario, String input, Boolean expectedMatched) {
        this.scenario = scenario;
        this.input = input;
        this.expectedMatched = expectedMatched;
    }

    @Parameterized.Parameters(name = "{index}: {1} should return {2}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {
                        "Single open bracket should return false",
                        "(",
                        false
                },
                {
                        "Single close bracket should return false",
                        ")",
                        false
                },
                {
                        "A pair of brackets should return true",
                        "()",
                        true
                },
                {
                        "Inverted pair of brackets should return false",
                        ")(",
                        false
                },
                {
                        "Unmatched type pair of brackets should return false",
                        "(]",
                        false
                },
                {
                        "Sentence with a bracketted segment should return true",
                        "This is a sentence (with something withing brackets) ok?",
                        true
                },
                {
                        "Sentence with nested bracketted segments should return true",
                        "Check this: {This is a sentence (with something withing [square] brackets) ok?}",
                        true
                },
                {
                        "Sentence with nested bracketted segments should return true",
                        "Check this: {This is a sentence (with something withing [square] brackets) ok?}",
                        true
                },
                {
                        "Sentence with nested non matching bracketted segments should return true",
                        "Check this: {This is a sentence (with something withing [square} brackets] ok?}",
                        false
                },
                {
                        "Many nested brackets all matching pairs return true",
                        "([{(oops) {trying [to (be {tricky})]} }])",
                        true
                },
                {
                        "Many nested brackets one unbalanced return false",
                        "([{(oops) {trying [to (be {tricky}!]} }])",
                        false
                }
        });
    }

    @Test
    public void testScenario() {
        BalanceTestResult result = taskService.validateBrackets(input);

        assertThat(result, notNullValue());
        assertThat(result.getIsBalanced(), equalTo(expectedMatched));
        assertThat(result.getInput(), equalTo(input));
    }
}