package com.kriswick.angapi.test.factory;

import com.kriswick.angapi.db.entity.ToDoItemEntity;
import com.kriswick.angapi.model.ToDoItemAddRequest;
import com.kriswick.angapi.model.ToDoItemUpdateRequest;
import net.bytebuddy.utility.RandomString;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.util.Random;

public class ToDoTestObjectFactory {

    public static ToDoItemAddRequest toDoItemAddRequest(String text){
        return new ToDoItemAddRequest(text);
    }

    public static ToDoItemEntity toDoItemEntity(Long id) {
        ToDoItemEntity entity = toDoItemEntity();
        ReflectionTestUtils.setField(entity, "id", id);

        return entity;
    }

    public static ToDoItemEntity toDoItemEntity() {
        ToDoItemEntity entity = new ToDoItemEntity();
        ReflectionTestUtils.setField(entity, "id", new Random().nextLong());
        entity.setText(RandomString.make());
        entity.setCreatedAt(LocalDateTime.now());
        entity.setCompleted(false);

        return entity;
    }

    public static ToDoItemUpdateRequest toDoItemUpdateRequest(String text, Boolean completed){
        return new ToDoItemUpdateRequest(text, completed);
    }
}
