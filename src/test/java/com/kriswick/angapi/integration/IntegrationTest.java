package com.kriswick.angapi.integration;

import com.kriswick.angapi.Application;
import org.junit.experimental.categories.Category;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = {Application.class})
@AutoConfigureMockMvc
@Category(IntegrationTest.class)
@Transactional
public @interface IntegrationTest {
}
