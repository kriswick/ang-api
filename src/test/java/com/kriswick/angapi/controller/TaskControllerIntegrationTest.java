package com.kriswick.angapi.controller;

import com.kriswick.angapi.integration.IntegrationTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
public class TaskControllerIntegrationTest {

    private static final String LONG_TEXT_WITH_BRACKETS = "Do integration test with (long long long long) long long long long long long long long long text with [some brackets that are perfectly balanced you know], not long enough";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createTodoReturnsErrorIfTheTextIsTooLong() throws Exception {
        mockMvc.perform(get("/tasks/validateBrackets?input=" + LONG_TEXT_WITH_BRACKETS))
                .andExpect(status().isBadRequest())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.name", equalTo("ValidationError")))
                .andExpect(jsonPath("$.details", hasSize(1)))
                .andExpect(jsonPath("$.details[0].message",
                        equalTo("Must be between 1 and 100 chars long")))
                .andExpect(jsonPath("$.details[0].location", equalTo("params")))
                .andExpect(jsonPath("$.details[0].param", equalTo("")))
                .andExpect(jsonPath("$.details[0].value", equalTo(LONG_TEXT_WITH_BRACKETS)));
    }

    @Test
    public void createTodoReturnsValidResponseForValidInput() throws Exception {
        mockMvc.perform(get("/tasks/validateBrackets?input=[{}]"))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.input", equalTo("[{}]")))
                .andExpect(jsonPath("$.isBalanced", equalTo(true)));
    }
}
