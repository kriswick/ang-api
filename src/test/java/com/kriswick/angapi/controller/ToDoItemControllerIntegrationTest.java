package com.kriswick.angapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.kriswick.angapi.integration.IntegrationTest;
import com.kriswick.angapi.model.ToDoItem;
import com.kriswick.angapi.model.ToDoItemAddRequest;
import com.kriswick.angapi.model.ToDoItemUpdateRequest;
import com.kriswick.angapi.service.ToDoItemService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.time.format.DateTimeFormatter;

import static com.kriswick.angapi.test.factory.ToDoTestObjectFactory.toDoItemAddRequest;
import static com.kriswick.angapi.test.factory.ToDoTestObjectFactory.toDoItemUpdateRequest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
public class ToDoItemControllerIntegrationTest {

    private static final String LONG_TEXT = "Do integration test with long long long long long long" +
            "long long long long long long long text";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ToDoItemService toDoItemService;

    @Test
    public void createTodoReturnsCreatedTodo() throws Exception {
        ToDoItemAddRequest request = toDoItemAddRequest("Do integration test");
        mockMvc.perform(post("/todo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(request)))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.text", equalTo(request.getText())))
                .andExpect(jsonPath("$.id", notNullValue()))
                .andExpect(jsonPath("$.isCompleted", equalTo(false)))
                .andExpect(jsonPath("$.createdAt", notNullValue()));
    }

    @Test
    public void createTodoReturnsErrorIfTheTextIsTooLong() throws Exception {
        ToDoItemAddRequest request = toDoItemAddRequest(LONG_TEXT);
        mockMvc.perform(post("/todo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.name", equalTo("ValidationError")))
                .andExpect(jsonPath("$.details", hasSize(1)))
                .andExpect(jsonPath("$.details[0].message",
                        equalTo("Must be between 1 and 50 chars long")))
                .andExpect(jsonPath("$.details[0].location", equalTo("params")))
                .andExpect(jsonPath("$.details[0].param", equalTo("text")))
                .andExpect(jsonPath("$.details[0].value", equalTo(request.getText())));
    }

    @Test
    public void getTodoReturnsErrorIfThereIsNoToDoItemWithGivenId() throws Exception {
        mockMvc.perform(get("/todo/666"))
                .andExpect(status().isNotFound())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.name", equalTo("NotFoundError")))
                .andExpect(jsonPath("$.details", hasSize(1)))
                .andExpect(jsonPath("$.details[0].message", equalTo("Item with id 666 not found")));
    }

    @Test
    public void getTodoReturnsValidToDoItemWhenFound() throws Exception {
        ToDoItem toDoItem = createTodoItem();
        mockMvc.perform(get("/todo/" + toDoItem.getId()))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.text", equalTo(toDoItem.getText())))
                .andExpect(jsonPath("$.id", equalTo(toDoItem.getId().intValue())))
                .andExpect(jsonPath("$.isCompleted", equalTo(toDoItem.getIsCompleted())))
                .andExpect(jsonPath("$.createdAt",
                        equalTo(toDoItem.getCreatedAt().format(DateTimeFormatter.ISO_DATE_TIME))));
    }

    @Test
    public void updateTodoReturnsErrorIfThereIsNoToDoItemWithGivenId() throws Exception {
        ToDoItemUpdateRequest updateRequest = toDoItemUpdateRequest("Updated", true);
        mockMvc.perform(patch("/todo/666")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(updateRequest)))
                .andExpect(status().isNotFound())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.name", equalTo("NotFoundError")))
                .andExpect(jsonPath("$.details", hasSize(1)))
                .andExpect(jsonPath("$.details[0].message", equalTo("Item with id 666 not found")));
    }

    @Test
    public void updateTodoReturnsErrorIfTheRequestIsInvalid() throws Exception {
        ToDoItem todoItem = createTodoItem();
        ToDoItemUpdateRequest updateRequest = toDoItemUpdateRequest(LONG_TEXT, true);
        mockMvc.perform(patch("/todo/" + todoItem.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(updateRequest)))
                .andExpect(status().isBadRequest())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.name", equalTo("ValidationError")))
                .andExpect(jsonPath("$.details", hasSize(1)))
                .andExpect(jsonPath("$.details[0].message",
                        equalTo("Must be between 1 and 50 chars long")))
                .andExpect(jsonPath("$.details[0].location", equalTo("params")))
                .andExpect(jsonPath("$.details[0].param", equalTo("text")))
                .andExpect(jsonPath("$.details[0].value", equalTo(updateRequest.getText())));
    }

    @Test
    public void updateTodoReturnsUpdateToDoItemIfTheRequestIsValid() throws Exception {
        ToDoItem todoItem = createTodoItem();
        ToDoItemUpdateRequest updateRequest = toDoItemUpdateRequest("Updated", true);
        mockMvc.perform(patch("/todo/" + todoItem.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(updateRequest)))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.text", equalTo(updateRequest.getText())))
                .andExpect(jsonPath("$.id", equalTo(todoItem.getId().intValue())))
                .andExpect(jsonPath("$.isCompleted", equalTo(updateRequest.getIsCompleted())))
                .andExpect(jsonPath("$.createdAt",
                        equalTo(todoItem.getCreatedAt().format(DateTimeFormatter.ISO_DATE_TIME))));
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    private ToDoItem createTodoItem(){
        return toDoItemService.createToDoItem(toDoItemAddRequest("Do integration test"));
    }
}
