# ang-api

This is a simple REST api implementing the specification in https://join.autogeneral.com.au/

Implementation of the API is available at http://ang-api.ap-southeast-2.elasticbeanstalk.com.

## To run the API locally:
(You will require latest version of docker running - https://www.docker.com/get-started)

+ Clone this code base

+ Go to the project root folder

+ Start MySQL docker container - *docker-compose up*

+ Run ./gradlew bootRun